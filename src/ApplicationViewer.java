import javax.swing.*;

public class ApplicationViewer {
    public static void main(String[] args) {
        JFrame frame = new InvestmentFrame3();
        frame.setTitle("Investment Application");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
