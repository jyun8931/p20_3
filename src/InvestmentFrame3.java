import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InvestmentFrame3 extends JFrame {
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 250;

    private static final int AREA_ROWS = 10;
    private static final int AREA_COLS = 30;

    private static final double DEFAULT_RATE = 5;
    private static final double INITIAL_BALANCE = 1000;

    private JLabel rateLabel;
    private JLabel balanceLabel;
    private JLabel yearsLabel;
    private JButton button;
    private JTextField rateField;
    private JTextField balanceField;
    private JTextField yearsField;
    private JTextArea resultArea;
    private double balance;

    public InvestmentFrame3(){
        resultArea = new JTextArea(AREA_ROWS, AREA_COLS);
        resultArea.setEditable(false);

        createTextField();
        createButton();
        createPanel();

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }
    private void createTextField(){
        final int FIELD_WIDTH = 10;

        rateLabel = new JLabel("Interest Rate:");
        rateField = new JTextField(FIELD_WIDTH);

        balanceLabel = new JLabel("Balance:");
        balanceField = new JTextField(FIELD_WIDTH);

        yearsLabel = new JLabel("Years:");
        yearsField = new JTextField(FIELD_WIDTH);

    }
    private void createPanel(){
        JPanel panel = new JPanel();
        panel.add(rateLabel);
        panel.add(rateField);
        panel.add(balanceLabel);
        panel.add(balanceField);
        panel.add(yearsLabel);
        panel.add(yearsField);

        panel.add(button);
        JScrollPane scrollPane = new JScrollPane(resultArea);
        panel.add(scrollPane);
        add(panel);
    }

    class AddInterestListener implements ActionListener {
        public void actionPerformed(ActionEvent event){
            double rate = Double.parseDouble(rateField.getText());
            balance = Double.parseDouble(balanceField.getText());
            int years = Integer.parseInt(yearsField.getText());
            for(int i = 1; i <= years; i++){
                double interest = balance * rate / 100;
                balance = balance + interest;
                resultArea.append("Year #" + i + " balance:" + balance + "\n");
            }
            resultArea.append("\n");
        }
    }
    private void createButton(){
        button = new JButton("Calculate");

        ActionListener listener = new AddInterestListener();
        button.addActionListener(listener);
    }
}
